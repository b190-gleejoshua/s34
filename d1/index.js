// npm init
/*
	-triggering this command will prompt the user for different settings that will define the application
	-using this commands will make a "package.json" in our repo
	-package.json tracks the version of our applicatio , depending in the settings we have set. also, we can see the dependencies that we have installed inside of  this file
*/

/*
	npm install express
		after triggering this command, express will now be 	listed as "dependecies".this can be inside "package.json"

		installing any dependency using npm will result to "node_modules" folder and package-lock.json
			"node_modules" directory should be left on the local repository because some of the hosting sites will fail to load the repository once it found the "node_modules" inside the repo. Another case where "node_modules" is left on the local repository is it takes too much time to commit
			"node_modules" is also where the dependencies needed files are stored

	".gitignore" files, will tell the git what files to be spare/ignored in terms of commiting and pushing

	"npm install" - this command is used when there are available dependencies inside our "package.json" but are not yet installed inside the repo/project
*/



// we need now the express module since in this dependency it has already built in codes that will allow the dev to create server in a breeze
const express = require("express");

// express() - allows us to create our server using express 
// simply put, the app variable is our server
const app=express();

// setting up port variaable
const port = 3000;

// app.use lets the server to handle data from requests
// "app.use(express.json())"; this allows the app to read and handle json data types
// methods used from express middlewares
	// middleware - software that provide common services and capabilities for the application
app.use(express.json());
// "app.use(express.urlencoded({extended: true}));" allows the server to read data from forms
// by default, information received from the url can only be received as a string or an array
// but using extended: true for the argument allows us to receive information from other data types such as objects which we will use throughout our application
app.use(express.urlencoded({extended: true}));


// SECTION-ROUTES
// GET method
/*
	Express has methods corresponding to each HTTP methods
	the route below expects to receive a GET request at the base URI "/"
*/
app.get("/",(req,res)=>{
	res.send("Hello World");
});

app.get("/hello",(req, res)=>{
	res.send("Hello from/hello endpoint")
});

// POST method
// this route expects to receive a POST request at the URI "/hello"
app.post("/hello", (req, res)=>{
	// req.body contains the contents/data of the request body
	// the properties defined in Postman request will be acessible here as properties with the same name
	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}!`);
});

let users=[]

app.post("/signup",(req, res)=>{
	console.log(req.body);
	if (req.body.firstName !== "" && req.body.lastName !== ""){
		users.push(req.body)
		res.send(`User ${req.body.firstName} ${req.body.lastName} has signed up successfully!`);
		console.log(users)
	}
	else{
		res.send("firstName and lastName should be filled with information");
	};

}); 

app.put("/change-lastName", (req, res)=>{
	let message;

	for (let i = 0; i<=users.length; i++){
		// checks if the user is existing in the array
		if(req.body.firstName==users[i].firstName){
			// will change the value of the lastName into a new lastName received from the request object
			users[i].lastName = req.body.lastName;

			message = `User ${req.body.firstName} has successfully changed her  lastName to ${req.body.lastName}`
			console.log(users);
			// will terminate the loop if there is a match
			break
		// if user is not found
		}else{
			message = "User does not exist"
		}
	};
	res.send(message);
});

// SECTION ACTIVITY
// Create a GET route that will access the "/home" route that will print out a simple message.
app.get("/home",(req,res)=>{
	res.send("Welcome to the home page");
});

// Create a GET route that will access the "/users" route that will retrieve all the users in the mock database.
app.post("/users",(req, res)=>{
	if (req.body.username !== "" && req.body.password !== ""){
		users.push(req.body)
		res.send(`User ${req.body.username} has signed up successfully!`);
	}
	else{
		res.send("firstName and lastName should be filled with information");
	};

}); 

app.get("/users", (req, res)=>{
	res.send(users)
})

 // Create a DELETE route that will access the "/delete-user" route to remove a user from the mock database.
app.delete("/delete-users",(req, res)=>{
	res.send(`User ${req.body.username}	has been deleted`)
})



app.listen(port, () => console.log(`Server running at port: ${port}`));


